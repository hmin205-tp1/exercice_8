package com.example.tp1_8;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    class CityList  extends BaseAdapter {
        List<dataLine> dataSource;

        public CityList(List<dataLine> data) {
            //super(MainActivity.this, R.layout.row, data);
            dataSource = data;
        }

        public void setData(List<dataLine> d) {
            dataSource = d;
        }

        @Override
        public int getCount() {
            return dataSource.size();
        }

        @Override
        public Object getItem(int position) {
            return dataSource.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public void updateCityList(List<dataLine> newlist) {
            dataSource.clear();
            dataSource.addAll(newlist);
            this.notifyDataSetChanged();
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            dataLine c = dataSource.get(position);
            convertView = LayoutInflater.from(MainActivity.this).inflate(R.layout.line,null);

            TextView cityA= (TextView) convertView.findViewById(R.id.cityA);
            TextView cityB= (TextView) convertView.findViewById(R.id.cityB);
            TextView hourA= (TextView) convertView.findViewById(R.id.hourA);
            TextView hourB= (TextView) convertView.findViewById(R.id.hourB);

            cityA.setText(c.getCity1());
            cityB.setText(c.getCity2());
            hourA.setText(c.getHour1());
            hourB.setText(c.getHour2());
            return convertView;
        }
    }


    CityList CL=null;

    private void adf(List<dataLine> dLL)
    {
        dLL.add(new dataLine("Montpellier","Thimphu","00:00","23:00"));
        dLL.add(new dataLine("Montpellier","Thimphu","11:00","03:00"));
        dLL.add(new dataLine("Montpellier","Thimphu","15:00","10:00"));
        dLL.add(new dataLine("Montpellier","Thimphu","20:00","15:00"));

        dLL.add(new dataLine("Paris","Thimphu","00:00","23:00"));
        dLL.add(new dataLine("Paris","Thimphu","11:00","03:00"));
        dLL.add(new dataLine("Paris","Thimphu","15:00","10:00"));
        dLL.add(new dataLine("Paris","Thimphu","20:00","15:00"));

        dLL.add(new dataLine("Paris","Ulm","00:00","23:00"));
        dLL.add(new dataLine("Paris","Ulm","11:00","03:00"));
        dLL.add(new dataLine("Paris","Ulm","15:00","10:00"));
        dLL.add(new dataLine("Paris","Ulm","20:00","15:00"));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText editA= (EditText) findViewById(R.id.editTextTextPersonName4);
        EditText editB= (EditText) findViewById(R.id.editTextTextPersonName3);

        List<dataLine> dLL= new ArrayList<>();
        List<dataLine> dLLn= new ArrayList<>();
        adf(dLL);
        adf(dLLn);
        /*Data*/



        /*End Data*/

        CL = new CityList(dLLn);

        ListView list = (ListView) findViewById(R.id.list);

        list.setAdapter(CL);

        Button button=(Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String villeD=;

                List<dataLine> NdLL= new ArrayList<>();
                for(int i=0;i<dLL.size();i++)
                {

                    //Log.println(Log.INFO,"Cool",editB.getText().toString()+dLL.get(i));
                    if(!editA.getText().toString().equals("")) {
                        if (editA.getText().toString().equals(dLL.get(i).getCity1()))
                        {
                            if(!editB.getText().toString().equals(""))
                            {
                                if (editB.getText().toString().equals(dLL.get(i).getCity2())) {
                                    NdLL.add(dLL.get(i));
                                }
                            }
                            else{
                                NdLL.add(dLL.get(i));
                            }
                        }
                    }
                    else
                    {
                        if(!editB.getText().toString().equals(""))
                        {
                            if (editB.getText().toString().equals(dLL.get(i).getCity2())) {
                                NdLL.add(dLL.get(i));
                            }
                        }
                        else{
                            NdLL.add(dLL.get(i));
                        }
                    }
                }
                CL.updateCityList(NdLL);
            }
        });
    }
}