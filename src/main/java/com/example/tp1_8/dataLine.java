package com.example.tp1_8;

public class dataLine {
    private String city1;
    private String city2;
    private String hour1;
    private String hour2;
    public dataLine(String cityA,String cityB,String hourA,String hourB)
    {
        city1=cityA;
        city2=cityB;
        hour1=hourA;
        hour2=hourB;
    }
    public String getCity1()
    {
        return city1;
    }
    public String getCity2()
    {
        return city2;
    }
    public String getHour1()
    {
        return hour1;
    }
    public String getHour2()
    {
        return hour2;
    }
}
